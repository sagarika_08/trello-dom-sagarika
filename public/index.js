
const API_KEY = "619273b2fa1dd6396421206fccac2e4e"
const TOKEN = "748727f5d63950b7185945cd7064df83002dedecfe65d77ab4f028c751fd1d03"
const BOARDS = []
// API
//GET BOARDS DETAILS

function getBoardDetails(){
    const BOARDS = []
    fetch(`https://api.trello.com/1/members/me/boards?key=${API_KEY}&token=${TOKEN}`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json'
        }
    })
        .then(response => {
            console.log(
                `Response GET BOARDS DETAILS: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(responseJSON => {
            console.log(responseJSON);
            responseJSON.forEach(response => {
    
                BOARDS.push(
    
                    {
                        "name": response.name,
                        "desc":response.desc, 
                        "id":response.id, 
                        "url":response.url,
                        "prefs": {
                            "background": response.prefs.background,
                            "backgroundImage": response.prefs.backgroundImage,
                            "backgroundBrightness": response.prefs.backgroundBrightness,
                            "backgroundColor":response.prefs.backgroundColor,
                        }
                    }
                )
            
            });
            return BOARDS
        })
        .then((BOARDS)=>{
            createBoards(BOARDS)
        })
        .catch(err => console.error(err));
    
    
}
getBoardDetails()
function  createNewBoard(board_name){
fetch(`https://api.trello.com/1/boards/?key=${API_KEY}&token=${TOKEN}&name=${board_name}`, {
  method: 'POST'
})
  .then(response => {
    console.log(
      `Response FROM CREATE BOARD: ${response.status} ${response.statusText}`
    );
    return response.text();
  })
  .then(text => getBoardDetails())
  .catch(err => console.error(err));
}
function deleteBoard(id){
fetch(`https://api.trello.com/1/boards/${id}?key=${API_KEY}&token=${TOKEN}`, {
  method: 'DELETE'
})
  .then(response => {
    console.log(
      `Response from deleteBoard: ${response.status} ${response.statusText}`
    );
    return response;
  })
  .then(response=>{
    getBoardDetails()
  })
  .catch(err => console.error(err));
}





function  createBoards(BOARDS){

    const boardArea = document.getElementById('boardArea')
    boardArea.innerHTML=''
    BOARDS.forEach(board=>{
        const eachBoardDiv = document.createElement('div')
        eachBoardDiv.classList.add('card')
        eachBoardDiv.classList.add('eachBoardDiv')
        eachBoardDiv.id=board.id
    
        const cardBody = document.createElement('div')
        cardBody.classList.add('card-body')
    
        const deleteDiv = document.createElement('div')
        deleteDiv.classList.add('deleteDiv')
        const icon = document.createElement('i')
        icon.classList.add('fas')
        icon.classList.add('fa-trash-alt')
        icon.classList.add('deleteicon')
        icon.classList.add('fa-lg')
        icon.id=board.id
        deleteDiv.appendChild(icon)
    
        const h5 = document.createElement('h5')
        h5.classList.add('card-title')
        h5.classList.add('boardTitle')
        h5.innerText = board.name
        console.log(board.name)
    
        cardBody.appendChild(deleteDiv)
        cardBody.appendChild(h5)
    
        eachBoardDiv.appendChild(cardBody)
        boardArea.appendChild(eachBoardDiv)
    })
   

}
   
document.getElementById('boardArea').addEventListener('click',(e)=>{
    let boardName = ''
    if(e.target.classList.contains('deleteicon')){
        boardName = e.target.parentElement.nextSibling.textContent
        const confirmResult = confirm(`Do you want to delete this ${boardName} ?`);
        if(confirmResult == true) {
             deleteBoard(e.target.id)
        }
       
    }
    if(e.target.classList.contains('card-body'))
    {
        let boardID = ''
        boardID = e.target.parentElement.id
        window.document.location = 'boards.html'+ '?boardId='+boardID
        console.log('clicked',e.target,e.target.parentElement.id)
    }
       
    if(e.target.classList.contains('boardTitle'))
    {
        let boardID = ''
        boardID = e.target.parentElement.parentElement.id
        window.document.location = 'boards.html'+ '?boardId='+boardID
        console.log('clicked',e.target,e.target.parentElement.parentElement.id)
    }
       
})

document.getElementById('addNewBoardForm').addEventListener('submit',(e)=>{
    let board_name  = document.getElementById('BoardName').value
    if(board_name!=''){
        document.getElementById('addNewBoardForm').reset()
        createNewBoard(board_name)
    }
})
  