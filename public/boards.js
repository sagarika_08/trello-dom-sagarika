let inputBoardName = document.getElementById('boardName');
let boardName
const API_KEY = "619273b2fa1dd6396421206fccac2e4e"
const TOKEN = "748727f5d63950b7185945cd7064df83002dedecfe65d77ab4f028c751fd1d03"
// const boardID = "6065bb9a44060909e2ec8360"
// const board = "TRELLOBOARD_CLONE_2"
let cards = []
let lists = []
let checklist = []
let checkItem = []
let CARD_NAME, IDLIST, LISTNAME, DESC, CARD_ID
let boardID = document.location.search.replace(/^.*?\=/,'')

// ----------------------------------API----------------------------------------------------


//GET BOARD DETAILS
fetch(`https://api.trello.com/1/boards/${boardID}?key=${API_KEY}&token=${TOKEN}`, {
    method: 'GET',
    headers: {
        'Accept': 'application/json'
    }
})
    .then(response => {
        console.log(
            `Response GET BOARD DETAILS: ${response.status} ${response.statusText}`
        );
        return response.json();
    })
    .then(response => {
        boardName = response.name
        inputBoardName.value = boardName;
    })
    .catch(err => console.error(err));


//GET LISTS ON A BOARD
function getLists() {
    cards = []
    lists = []
    checklist = []
    fetch(`https://api.trello.com/1/boards/${boardID}/lists?key=${API_KEY}&token=${TOKEN}`, {
        method: 'GET'
    })
        .then(response => {
            console.log(`Response FROM GET LIST: ${response.status} ${response.statusText}`);
            return response.json();
        })
        .then(responseJSON => {
            responseJSON.forEach(list => {
                lists.push({ "idBoard": list.idBoard, "listId": list.id, "name": list.name })
            })
            return lists
        })
        .then(lists => {
            if (lists.length > 0) {
                getCards()
            }
            else {
                render()
            }
        })
        .catch(err => console.error(err));
}
getLists()
//GET CARDS
function getCards() {
    cards = []
    fetch(`https://api.trello.com/1/boards/${boardID}/cards?key=${API_KEY}&token=${TOKEN}`, {
        method: 'GET'
    })
        .then(response => {
            console.log(
                `Response CARDS: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(responseJSON => {
            responseJSON.forEach(card => {
                cards.push({ "idBoard": card.idBoard, "idList": card.idList, "card_id": card.id, "name": card.name, "idChecklists": card.idChecklists, "checkItemStates": card.checkItemStates, 'desc': card.desc })
            })
            return cards
        })
        .then(cards => {
            render()
        })
        .catch(err => console.error(err));
}
//GETTING CHECKLIST
function getCheckLists(cardResult) {
    checklist = []
            fetch(`https://api.trello.com/1/boards/${boardID}/checklists?key=${API_KEY}&token=${TOKEN}`, {
                method: 'GET'
            })
                .then(response => {
                    console.log(`Response FROM GET  CHECKLIST: ${response.status} ${response.statusText}`);
                    return response.json();
                })
                .then(responseJSON => {
                    responseJSON.forEach(response => {
                        checklist.push({ "idCard": response.idCard, "idBoard": response.idBoard, "id": response.id, "name": response.name, "checkitems": response.checkItems })
                    })
                    return checklist
                })
                .then(checklist => {
                    if (checklist.length > 0)
                        getCheckListItem(checklist)
                    else
                        render()
                })
}
//GET CHECK ITEMS
function getCheckListItem(checklists) {
    checkItem = []
    checklists.forEach(checklist => {
        if (checklist.idCard == CARD_ID) {
            fetch(`https://api.trello.com/1/checklists/${checklist.id}/checkItems?key=${API_KEY}&token=${TOKEN}`, {
                method: 'GET'
            })
                .then(response => {
                    console.log(`Response FROM GET CHECK LIST ITEM: ${response.status} ${response.statusText}`);
                    return response.json();
                })
                .then(responseJSON => {
                    if (responseJSON.length > 0) {
                        responseJSON.forEach(item => {
                            checkItem.push({ "id": item.id, "idChecklist": item.idChecklist, "idMember": item.idMember, "name": item.name, "nameData": item.nameData, "pos": item.pos, "state": item.state })
                        })
                        return checkItem
                    }
                    else {
                        showCheckList()
                    }
                })

                .then((checkItem) => {
                    showCheckList()
                })
                .catch(err => console.error(err));
        }
        else {
            showCheckList()
        }
    })


}
//CREATE NEW LIST
function createList(listName) {
    fetch(`https://api.trello.com/1/boards/${boardID}/lists?key=${API_KEY}&token=${TOKEN}&name=${listName}`, {
        method: 'POST'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(response => getLists())
        .catch(err => console.error(err));
}
//CREATE CARD
function createCard(cardName, listId) {
    fetch(`https://api.trello.com/1/cards?name=${cardName}&key=${API_KEY}&token=${TOKEN}&idList=${listId}`, {
        method: 'POST'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.text();
        })
        .then(response => getLists())
        .catch(err => console.error(err));
}
//DELETE A CARD FROM A LIST
function deleteCard(card_id) {
    fetch(`https://api.trello.com/1/cards/${card_id}?key=${API_KEY}&token=${TOKEN}`, {
        method: 'DELETE'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.text();
        })

        .then(response => {
            getLists()
        })
        .catch(err => console.error(err));
}
//UPDATE CARD
function updateCard(cardID, data) {
    if (data.name) {
        fetch(`https://api.trello.com/1/cards/${cardID}?name=${data.name}&key=${API_KEY}&token=${TOKEN}`, {
            method: 'PUT',
            headers: { 'Accept': 'application/json' }
        })
            .then(response => {
                console.log(`Response from UPDATE CARD: ${response.status} ${response.statusText}`);
                return response.text();
            })
            .then(response => getLists())
            .catch(err => console.error('ERROR FROM UPDATE', err));
    }
    else {
        fetch(`https://api.trello.com/1/cards/${cardID}?desc=${data.desc}&key=${API_KEY}&token=${TOKEN}`, {
            method: 'PUT',
            headers: { 'Accept': 'application/json' }
        })
            .then(response => {
                return response.text();
            })
            .then(response => {

                document.getElementById('descriptionText').innerHTML = data.desc
                getLists()
            })
            .catch(err => console.error('ERROR FROM UPDATE', err));
    }
}
//CREATE CHECK LIST
function createCheckList(cardId, name) {
    fetch(`https://api.trello.com/1/cards/${cardId}/checklists?name=${name}&key=${API_KEY}&token=${TOKEN}`, {
        method: 'POST'
    })
        .then(response => {
            console.log(`Response FROM CREATE CHECKLIST: ${response.status} ${response.statusText}`);
            return response.json();
        })
        .then(responseJSON => {
            getCheckLists(cards)
        })
        .catch(err => console.error(err));
}
//CREATE CHECKLIST ITEM 
function createCheckItem(checklistId, name) {
    fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${API_KEY}&token=${TOKEN}&name=${name}`, {
        method: 'POST'
    })
        .then(response => {
            console.log(`Response FROM ADDING CHECK ITEM: ${response.status} ${response.statusText}`);
            return response.json();
        })
        .then(responseJSON =>
            getCheckLists(cards))
        .catch(err => console.error(err));
}
//DELETE A CHECKLIST
function deleteChecklist(checkListId) {
    fetch(`https://api.trello.com/1/checklists/${checkListId}?key=${API_KEY}&token=${TOKEN}`, {
        method: 'DELETE'
    })
        .then(response => {
            console.log(
                `Response FROM DELETE CHECKLIST: ${response.status} ${response.statusText}`
            );
            return response.json()
        })
        .then(responseJSON => getCheckLists(cards))
        .catch(err => console.error(err));

}
//DELETE CHECK ITEM
function deleteCheckItem(checkListId, checkItemId) {
    fetch(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${API_KEY}&token=${TOKEN}`, {
        method: 'DELETE'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json;
        })
        .then(responseJSON => getCheckLists(cards))
        .catch(err => console.error(err));
}
//UPDATE CHECKLIST ITEM
function updateCheckList(CARD_ID,checkListId,checkListItemId,UpdatedName){
    fetch(`https://api.trello.com/1/cards/${CARD_ID}/checklist/${checkListId}/checkItem/${checkListItemId}?name=${UpdatedName}&key=${API_KEY}&token=${TOKEN}`,{
        method: 'PUT'

    })
    .then(response => {
        console.log(
            `Response FROM UPDATE CHECKLIST ITEM: ${response.status} ${response.statusText}`
        );
        return response.json;
    })
    .then(responseJSON => getCheckLists(cards))
    .catch(err => console.error(err));
}


// ----------------------------------API ENDS----------------------------------------------------



// ------------------------------Functions and EVEN LISTENERS---------------------------------------------


function render() {
    CARD_NAME = '', IDLIST = '', LISTNAME = '', DESC = ''
    const ListArea = document.getElementById('ListArea')
    ListArea.innerHTML = ''

    if (lists.length > 0) {
        lists.forEach(list => {
            //list
            const createList = document.createElement('div')
            createList.classList.add('card')
            createList.id = "LISTS"
            //card-body
            const card_body = document.createElement('div')
            card_body.classList.add('card-body')
            const h5 = document.createElement('h5')
            h5.classList.add('card-title')
            h5.id = list.listId
            h5.textContent = list.name
            const menuBtn = document.createElement('div')
            menuBtn.innerHTML = `<i class="fal fa-ellipsis-h menu-icon" id="menu-icon"></i>`
            card_body.appendChild(h5)
            card_body.appendChild(menuBtn)
            createList.appendChild(card_body)
            //ul
            const ulList = document.createElement('ul')
            ulList.classList.add('list-group')
            const card_footer = document.createElement('div')
            card_footer.classList.add('card-footer')
            const aLink = document.createElement('a')
            aLink.href = "#"
            aLink.classList.add('add-card')
            aLink.id = "add-card"
            cards.forEach(card => {
                if (list.listId == card.idList) {
                    const createLi = document.createElement('li')
                    createLi.classList.add('list-group-item')
                    // createLi.id = "card_item"
                    createLi.id = card.card_id
                    createLi.textContent = card.name
                    const btnDiv = document.createElement('div')
                    btnDiv.classList.add('btnDiv')
                    btnDiv.id = "btnDiv"
                    //deletebtn
                    const deleteButton = document.createElement('div')
                    deleteButton.classList.add('deleteButton')
                    deleteButton.id = "deleteButton"
                    deleteButton.innerHTML = `<i class="far fa-trash-alt deleteIcon"></i> `
                    //edit btn 
                    const editBtn = document.createElement('div')
                    editBtn.classList.add('editBtn')
                    editBtn.id = "editBtn"
                    editBtn.innerHTML = `<i class="fas fa-pen editIcon"></i>`
                    btnDiv.appendChild(editBtn)
                    btnDiv.appendChild(deleteButton)
                    createLi.appendChild(btnDiv)
                    ulList.appendChild(createLi)
                    aLink.innerHTML = '<i class="fal fa-plus">&nbsp;&nbsp;</i>Add another card';

                }

            })
            if (aLink.innerHTML == '')
                aLink.innerHTML = '<i class="fal fa-plus">&nbsp;&nbsp;</i>Add card';
            createList.appendChild(ulList)
            //card footer
            card_footer.appendChild(aLink);
            createList.appendChild(card_footer)
            ListArea.appendChild(createList)
            document.getElementById('menu-icon').addEventListener('click', showMenu)
        });
    }
}
render();

//add another List button
document.getElementById('addList').addEventListener('click', (e) => {
    e.preventDefault()
    document.getElementById('AddListDiv').style.display = 'block'
})
//add list form event listener -CREATE LIST
document.getElementById('formAddList').addEventListener('submit', (e) => {
    e.preventDefault()
    const listName = document.getElementById('addListinput').value
    if (listName != '') {
        document.getElementById('AddListDiv').style.display = 'none'
        createList(listName)
    }

})
//add,edit,delete card ,show check list modal
document.getElementById('ListArea').addEventListener('click', (e) => {
    
    e.preventDefault()
    const area = e.target.parentElement.parentElement.parentElement
    console.log('TARGET ',e.target)
    const listArea = document.getElementById('ListArea')
    if (e.target.classList.contains('add-card')) {
        const parentFooterDiv = e.target.parentElement;
        const formForCard = `<form action="" class="formAddCard" id="formAddCard">
                                    <textarea class="form-control" onkeyup="textAreaAdjust(this)"  
                                        id="addCardinput"  
                                         placeholder="Enter card title..."></textarea>
                                    <button type="submit" class="btn" id="addCardBtn">Add card</button>
                            </form>`
        parentFooterDiv.innerHTML = formForCard

        //add card form event
        document.getElementById('addCardBtn').addEventListener('click', e => {
            let topDiv = e.target.parentElement.parentElement.previousSibling.previousSibling.children
            listId = topDiv[0].id
            const textAreaValue = document.getElementById('addCardinput').value
            if (textAreaValue != '')
                createCard(textAreaValue, listId)
            render()
        })
    }
    else if (e.target.classList.contains('editIcon')) {
        cardItem = document.getElementById(e.target.parentElement.parentElement.parentElement.id)
        const parentCard = e.target.parentElement.parentElement.parentElement
        const formForCard = `<form action="" class="formAddCard" id="formAddCard">
                                    <textarea class="form-control" onkeyup="textAreaAdjust(this)"  
                                    id="EditCardinput">${parentCard.textContent}</textarea>
                                    <button type="submit" class="btn" id= "editCardBtn">save</button>
                             </form>`
        parentCard.innerHTML = formForCard

        document.getElementById("editCardBtn").addEventListener('click', (e) => {
            let data = { "name": document.getElementById('EditCardinput').value }
            updateCard(cardItem.id, data)
        })
    }
    else if (e.target.classList.contains('deleteIcon')) {
        const userReply = confirm("Do you want to remove this card ?");
        if (userReply == true)
            deleteCard(e.target.parentElement.parentElement.parentElement.id)

    }
    else if (e.target.classList.contains('list-group-item')) {
        CARD_ID = ''
        CARD_NAME = ''
        document.getElementById('descriptionText').innerHTML = 'Add a more detailed description'
        CARD_ID = e.target.id
        for (let i of cards) {
            if (i.card_id == CARD_ID) {
                IDLIST = i.idList
                if (i.desc != '' || i.desc != undefined)
                    DESC = i.desc
                for (j of lists) {
                    if (j.listId == IDLIST) {
                        LISTNAME = j.name
                    }
                }
                CARD_NAME = i.name
            }
        }
        document.getElementById('card_name').innerHTML = CARD_NAME
        document.getElementById('card_name').dataset.CARD_ID = CARD_ID
        document.getElementById('listName').innerHTML = LISTNAME
        document.getElementById('listName').dataset.listId = IDLIST

        if (DESC != '') {
            document.getElementById('descriptionText').innerHTML = DESC
        }
        getCheckLists(cards)
        e.target.dataset.target = "#cardDetails"
        e.target.dataset.toggle = "modal"
    }
  
})
//add description in check list modal ,( update card description)
document.getElementById('descriptionText').addEventListener('click', (e) => {
    e.preventDefault()
    const descriptionText = document.getElementById('descriptionText')
    if (descriptionText.textContent == 'Add a more detailed description') {
        document.getElementById('descriptionText').innerHTML =
            `<form action="" class="formAddDesc" id="formAddDesc">
            <div class="form-group">
                <textarea row=4 type="text" class="form-control" id="AddDesc" placeholder="Enter card description here..."></textarea>
                <button type="submit" class="btn" id="AddDescBtn">save</button>
            </div>
            </form>`

        document.getElementById('AddDescBtn').addEventListener('click', (e) => {
            e.preventDefault()
            let data = { "desc": document.getElementById("AddDesc").value }
            document.getElementById('formAddDesc').reset()
            document.getElementById('formAddDesc').innerHTML = ''
            updateCard(CARD_ID, data)
        })

    }

})
//create checklist form 
document.getElementById('addCheckListForm').addEventListener('submit', (e) => {
    e.preventDefault()
    let NewCheckList = ''
    NewCheckList = document.getElementById('add_checklistName').value
    if (NewCheckList != '') {
        document.getElementById('addCheckListForm').reset()
        createCheckList(CARD_ID, NewCheckList)
    }
})
//add checklist item form
document.getElementById('addCheckListItemForm').addEventListener('submit', (e) => {
    e.preventDefault()
    let checkListId = e.target.add_checklItemBtn.getAttribute('data-add_checkl-item-btn')
    let newCheckItem=''
    newCheckItem = document.getElementById('add_checklistItemName').value
    console.log('new item',newCheckItem)
    if (newCheckItem != '') {
        console.log('calling createcheckitem')
        document.getElementById('addCheckListItemForm').reset()
        createCheckItem(checkListId, newCheckItem)
    }
})
//checklist modal listener (add item.delete item,delete check list,update  modal)
document.getElementById('checkLists').addEventListener('click', (e) => {
    console.log('TARGET' ,e.target.classList)
    if (e.target.id == 'addItemBtn') {
        const parentId = e.target.parentElement.parentElement.id
        const checkListId = parentId
        e.target.dataset.toggle = "modal"
        e.target.dataset.target = "#addChecklistItem"
        document.getElementById('add_checklItemBtn').dataset.add_checklItemBtn = parentId
    }

    else if (e.target.id == 'deleteBtn') {
        const checklistID = e.target.parentElement.parentElement.parentElement.id
        let checklistNAME
        for (let list of checklist) {
            if (list.id == checklistID) {
                checklistNAME = list.name
            }
        }
        document.getElementById('deleteChecklistLabel').innerHTML = `delete ${checklistNAME}`
        document.getElementById('delete_checklistBtn').dataset.delete_checklistBtn = checklistID
        e.target.dataset.toggle = "modal"
        e.target.dataset.target = "#deleteChecklist"
    }
    else if (e.target.classList.contains("deleteIcon")) {
        //delete checklist 
        console.log('inside delete block', e.target)
        const checkListId = e.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.id
        const checkItemId = e.target.parentElement.parentElement.parentElement.id
        deleteCheckItem(checkListId, checkItemId)
    }
    else if (e.target.classList.contains('CheckListName')){
        let CheckItemId = ''
        let CheckListId = ''
        CheckListId = e.target.parentElement.parentElement.parentElement.parentElement.parentElement.id
        CheckItemId = e.target.parentElement.parentElement.id
        document.getElementById('update_checklItemBtn').dataset.update_checklItemBtn = CheckItemId
        document.getElementById('update_checklItemBtn').dataset.update_CheckListId = CheckListId

        document.getElementById('update_checklistItemName').value = e.target.textContent
        e.target.dataset.toggle='modal'
        e.target.dataset.target='#updateChecklistItem'

    }
})
//update check list item
document.getElementById('updateCheckListItemForm').addEventListener(('submit'),(e)=>{
    e.preventDefault()
    let checkListItemId = ''
    let checkListId = ''
    const UpdatedName = document.getElementById('update_checklistItemName').value
    checkListItemId = e.target.update_checklItemBtn.getAttribute('data-update_checkl-item-btn')
    checkListId = e.target.update_checklItemBtn.getAttribute('data-update_-check-list-id')
    document.getElementById('updateCheckListItemForm').reset()
    if(UpdatedName != '')
    {
        updateCheckList(CARD_ID,checkListId,checkListItemId,UpdatedName)
     }
    


})
//delete Check list
document.getElementById('deleteCheckListForm').addEventListener(('submit'), (e) => {
    e.preventDefault()
    const checkListId = e.target.delete_checklistBtn.getAttribute('data-delete_checklist-btn')
    deleteChecklist(checkListId)

})
//show checklist
function showCheckList() {
    console.log('inside showCheckList')
    document.getElementById('checkLists').innerHTML = ''
    for (let j of checklist) {
        if (j.idCard == CARD_ID) {
            const newCheckList = document.createElement('div')
            newCheckList.classList.add('eachChecklist')
            newCheckList.id = j.id

            //checkTitle
            const checkTitle = document.createElement('div')
            checkTitle.classList.add('checkTitle')
            checkTitle.id = 'checkTitle'

            const icon = document.createElement('i')
            icon.classList.add("fal")
            icon.classList.add("fa-check-square")
            icon.classList.add("first_sec_icon")

            const checklist_details = document.createElement('div')
            checklist_details.classList.add('checklist_details')
            checklist_details.id = 'checklist_details'
            const h5 = document.createElement('h5')
            h5.textContent = j.name
            h5.classList.add('checklist_title')
            h5.id = 'checklist_title'
            const deleteBtn = document.createElement('button')
            deleteBtn.classList.add('btn')
            deleteBtn.id = 'deleteBtn'
            deleteBtn.textContent = "Delete"

            checklist_details.appendChild(h5)
            checklist_details.appendChild(deleteBtn)
            checkTitle.appendChild(icon)
            checkTitle.appendChild(checklist_details)



            const checkItems = document.createElement('div')
            checkItems.classList.add('checkItems')
            checkItems.id = "checkItems"
            // const ul = document.createElement('ul')
            // ul.classList.add('list-group')
            // ul.classList.add('list-group-flush')
            // ul.id = "showItems"
            if (j.checkitems.length > 0) {
                for (item of checkItem) {
                    if (item.idChecklist == j.id) {

                        //ul
                        const ul = document.createElement('ul')
                        ul.classList.add('list-group')
                        ul.classList.add('list-group-flush')
                        ul.id = "showItems"

                        //li
                        const li = document.createElement('li')
                        li.classList.add('listItem')
                        li.id = item.id

                        //parent div of below 3
                        const inputLabel = document.createElement('div')
                        inputLabel.classList.add('inputLabel')

                        //checkbox
                        const checkbox = document.createElement('input')
                        checkbox.className = "form-check-input"
                        checkbox.id = 'checkbox'
                        checkbox.type = "checkbox"
                        checkbox.value = ''

                        //label
                        const label = document.createElement('label')
                        label.classList.add('form-check-label')
                        label.classList.add('CheckListName')
                        label.textContent = item.name
                        label.id = item.idChecklist
                        inputLabel.appendChild(checkbox)
                        inputLabel.appendChild(label)

                        //delete button icon
                        const dltBtn = document.createElement('div')
                        dltBtn.classList.add('dltBtn')
                        dltBtn.id = 'dltBtn'

                        const btn = document.createElement('button')
                        btn.classList.add('btn')
                        const icon = document.createElement('i')
                        icon.classList.add("far")
                        icon.classList.add("fa-trash-alt")
                        icon.classList.add("deleteIcon")
                        icon.id = "itemDelete"

                        btn.appendChild(icon)
                        dltBtn.appendChild(btn)

                        li.appendChild(inputLabel)
                        li.appendChild(dltBtn)
                        ul.appendChild(li)
                        checkItems.appendChild(ul)
                    }
                }
            }

            const addBtn = document.createElement('div')
            addBtn.classList.add('addBtn')
            addBtn.id = 'addBtn'
            const addItemBtn = document.createElement('button')
            addItemBtn.classList.add('addItemBtn')
            addItemBtn.id = 'addItemBtn'
            addItemBtn.innerText = 'Add an item'
            addBtn.appendChild(addItemBtn)


            newCheckList.appendChild(checkTitle)
            newCheckList.appendChild(checkItems)
            newCheckList.appendChild(addBtn)
            document.getElementById('checkLists').appendChild(newCheckList)


        }

    }
}


//show menu
function showMenu() {
    console.log('menu')
}
function textAreaAdjust(element) {
    element.style.height = "1px";
    element.style.height = (25 + element.scrollHeight) + "px";
}
inputBoardName.addEventListener('input', resizeInput);
resizeInput.call(inputBoardName);
function resizeInput() {
    this.style.width = this.value.length + 25 + "ch";
    inputBoardName.value = this.value
}